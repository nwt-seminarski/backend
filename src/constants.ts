import { IsNotEmpty, IsString } from 'class-validator';

export enum ApiPaths {
  USERS = 'api/users',
  MESSAGES = 'api/messages',
  AUTH = 'api/auth',
}

export enum ApiAuthPaths {
  LOGIN = 'login',
  SIGNUP = 'signup',
  VERIFY = 'verify',
}

export enum ApiMessagesPaths {}

export enum ApiUsersPaths {}

export enum SocketIncomingCommands {
  ACTIVE_USERS_SUB = 'active-users-sub',
  ACTIVE_USERS_UNSUB = 'active-users-unsub',
  ACTIVE_USERS_GET = 'active-users-get',
  LOAD_MESSAGES = 'load-messages',
  SEND_MESSAGE = 'send-message',
  INBOX_SUB = 'inbox-sub',
  INBOX_UNSUB = 'inbox-unsub',
  INBOX_GET = 'inbox-get',
  SEEN_MESSAGES = 'seen-messages',
  AUTH = 'auth',
  ALL_USERS_GET = 'all-users-get',
}

export enum SocketSubscriptions {
  ACTIVE_USERS = 'active-users',
  INBOX = 'inbox',
}

export enum SocketEmitCommands {
  ACTIVE_USERS = 'active-users',
  MESSAGES_NEW = 'messages-new',
  INBOX_UPDATE = 'inbox-update',
}
