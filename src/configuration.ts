const configuration = () => ({
  BACKEND_PORT: parseInt(process.env.BACKEND_PORT, 10),
  WS_PORT: parseInt(process.env.WS_PORT, 10),
  DATABASE_HOST: process.env.DATABASE_HOST,
  DATABASE_PORT: parseInt(process.env.DATABASE_PORT, 10),
  DATABASE_USER: process.env.DATABASE_USER,
  DATABASE_PASSWORD: process.env.DATABASE_PASSWORD,
  DATABASE_NAME: process.env.DATABASE_NAME,
  ENABLE_LOGGING: process.env.ENABLE_LOGGING,
  APPLICATION_NAME: process.env.APPLICATION_NAME,
  SALT_ROUNDS: parseInt(process.env.SALT_ROUNDS, 10),
  JWT_SECRET: process.env.JWT_SECRET,
  NODE_ENV: process.env.NODE_ENV,
});

export default configuration;
