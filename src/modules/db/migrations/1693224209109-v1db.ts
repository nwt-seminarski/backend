import { MigrationInterface, QueryRunner } from 'typeorm';

export class V1db1693224209109 implements MigrationInterface {
  name = 'V1db1693224209109';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "base" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "modified_at" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "deleted_at" TIMESTAMP, CONSTRAINT "PK_ee39d2f844e458c187af0e5383f" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_ee39d2f844e458c187af0e5383" ON "base" ("id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_0a279a7c73c8d7c9f2e7e80e67" ON "base" ("deleted_at") `,
    );
    await queryRunner.query(
      `CREATE TABLE "message_entity" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "modified_at" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "deleted_at" TIMESTAMP, "content" text NOT NULL, "seen_at" TIMESTAMP, "to_user_id" integer, "from_user_id" integer, CONSTRAINT "PK_45bb3707fbb99a73e831fee41e0" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_45bb3707fbb99a73e831fee41e" ON "message_entity" ("id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_fd78fe7dddb3f543a509e37760" ON "message_entity" ("deleted_at") `,
    );
    await queryRunner.query(
      `CREATE TABLE "user_entity" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "modified_at" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone, "deleted_at" TIMESTAMP, "username" character varying(512) NOT NULL DEFAULT 'John', "password" character varying(512) NOT NULL, CONSTRAINT "PK_b54f8ea623b17094db7667d8206" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_b54f8ea623b17094db7667d820" ON "user_entity" ("id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_43cf6e1528e4648bc3117035b9" ON "user_entity" ("deleted_at") `,
    );
    await queryRunner.query(
      `ALTER TABLE "message_entity" ADD CONSTRAINT "FK_6f06000f4ea688e7fe775ed4d40" FOREIGN KEY ("to_user_id") REFERENCES "user_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "message_entity" ADD CONSTRAINT "FK_1cae7e370395dcd997d8728fe53" FOREIGN KEY ("from_user_id") REFERENCES "user_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "message_entity" DROP CONSTRAINT "FK_1cae7e370395dcd997d8728fe53"`,
    );
    await queryRunner.query(
      `ALTER TABLE "message_entity" DROP CONSTRAINT "FK_6f06000f4ea688e7fe775ed4d40"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_43cf6e1528e4648bc3117035b9"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_b54f8ea623b17094db7667d820"`,
    );
    await queryRunner.query(`DROP TABLE "user_entity"`);
    await queryRunner.query(
      `DROP INDEX "public"."IDX_fd78fe7dddb3f543a509e37760"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_45bb3707fbb99a73e831fee41e"`,
    );
    await queryRunner.query(`DROP TABLE "message_entity"`);
    await queryRunner.query(
      `DROP INDEX "public"."IDX_0a279a7c73c8d7c9f2e7e80e67"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_ee39d2f844e458c187af0e5383"`,
    );
    await queryRunner.query(`DROP TABLE "base"`);
  }
}
