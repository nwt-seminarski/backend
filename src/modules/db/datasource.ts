import { join } from 'path';
import databaseCli from '@database-cli';
import { DataSource } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

const dataSource = new DataSource({
  type: 'postgres',
  host: databaseCli.host,
  port: databaseCli.port,
  username: databaseCli.user,
  password: databaseCli.password,
  database: databaseCli.databaseName,
  ssl: false,
  synchronize: false,
  entities: [join(__dirname, 'entities/*{.ts,.js}')],
  migrations: [join(__dirname, 'migrations/*{.ts,.js}')],
  migrationsRun: false,
  connectTimeoutMS: 5000,
  namingStrategy: new SnakeNamingStrategy(),
});

export default dataSource;
