import { Repository } from 'typeorm';
import { MessageEntity } from '../entities/message.entity';

export class MessageRepository extends Repository<MessageEntity> {}
