import configuration from '../../configuration';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { ConfigModule } from '@nestjs/config';
import { UserEntity } from './entities/user.entity';
import { MessageEntity } from './entities/message.entity';
import { BaseEntity } from 'typeorm';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: configuration().DATABASE_HOST,
      port: configuration().DATABASE_PORT,
      username: configuration().DATABASE_USER,
      password: configuration().DATABASE_PASSWORD,
      database: configuration().DATABASE_NAME,
      entities: [BaseEntity, UserEntity, MessageEntity],
      synchronize: false,
      migrationsRun: true,
      migrations: [join(__dirname, 'migrations/*{.ts,.js}')],
      autoLoadEntities: true,
      namingStrategy: new SnakeNamingStrategy(),
      applicationName: configuration().APPLICATION_NAME,
    }),
  ],
})
export class DatabaseModule {}
