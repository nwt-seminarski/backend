import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Base } from './base.entity';
import { UserEntity } from './user.entity';

@Entity()
export class MessageEntity extends Base {
  public static tableName = 'message_entity';

  @Column({ type: 'text', nullable: false })
  content: string;

  @Column({ type: 'timestamp', default: null })
  seenAt: Date | null;

  @ManyToOne(() => UserEntity, (userEntity) => userEntity.inbox)
  @JoinColumn({ name: 'to_user_id' })
  toUser: UserEntity;

  @ManyToOne(() => UserEntity, (userEntity) => userEntity.outbox)
  @JoinColumn({ name: 'from_user_id' })
  fromUser: UserEntity;
}
