import {
  AfterLoad,
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  OneToMany,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import configuration from '../../../configuration';
import { Base } from './base.entity';
import { MessageEntity } from './message.entity';

@Entity()
export class UserEntity extends Base {
  public static tableName = 'user_entity';

  private tempPassword: string;

  @Column({ type: 'varchar', length: 512, nullable: false, default: 'John' })
  username: string;

  @Column({ type: 'varchar', length: 512, nullable: false })
  password: string;

  @AfterLoad()
  async afterLoad() {
    this.tempPassword = this.password;
  }

  @BeforeInsert()
  @BeforeUpdate()
  async beforeInsertUpdate() {
    if (this.password !== this.tempPassword) {
      const hash = await bcrypt.hash(
        this.password,
        configuration().SALT_ROUNDS,
      );
      this.password = hash;
      this.tempPassword = this.password;
    }
  }

  @OneToMany(() => MessageEntity, (messageEntity) => messageEntity.toUser)
  inbox: MessageEntity[];

  @OneToMany(() => MessageEntity, (messageEntity) => messageEntity.fromUser)
  outbox: MessageEntity[];
}
