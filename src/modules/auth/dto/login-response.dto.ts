import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class UserLoginResponseDto {
  @IsNotEmpty()
  @IsString()
  access_token: string;

  @IsNotEmpty()
  @IsString()
  username: string;

  @IsNotEmpty()
  @IsNumber()
  userId: number;
}
