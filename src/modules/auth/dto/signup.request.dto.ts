import { IsNotEmpty, IsString } from 'class-validator';

export class AuthSignupRequestBodyDto {
  @IsNotEmpty()
  @IsString()
  username: string;

  @IsNotEmpty()
  @IsString()
  password: string;
}
