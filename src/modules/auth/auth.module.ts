import { Module } from '@nestjs/common';
import { UserAuthStrategy } from './auth.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';
import { UserJwtStrategy } from './jwt.strategy';
import { UserModule } from '../user/user.module';
import configuration from 'src/configuration';
import { AuthService } from './auth.service';
import { AuthApiController } from './auth.controller';

@Module({
  imports: [
    ConfigModule.forRoot(),
    UserModule,
    JwtModule.register({
      secret: configuration().JWT_SECRET,
      signOptions: { expiresIn: '8h' },
    }),
  ],
  providers: [AuthService, UserAuthStrategy, UserJwtStrategy],
  exports: [AuthService],
  controllers: [AuthApiController],
})
export class AuthModule {}
