export interface UserJwtTokenPayload {
  id: number;
  username: string;
}
