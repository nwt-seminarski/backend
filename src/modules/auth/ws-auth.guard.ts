import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthIncomingDto } from './dto/auth.incoming.dto';
import { AuthService } from './auth.service';
import { SocketWithUser } from 'src/common/types/request';
import { SocketIncomingCommands } from 'src/constants';

@Injectable()
export class WsUserJwtAuthGuard implements CanActivate {
  constructor(private readonly authService: AuthService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const client = context.switchToWs().getClient() as SocketWithUser;
    const data = context.switchToWs().getData();
    if (typeof data === 'object' && data.token) {
      const { token } = data as AuthIncomingDto;
      const payload = await this.authService.validateToken(token);
      if (payload === null) {
        client.emit(SocketIncomingCommands.AUTH, { success: false });
        return false;
      }
      if (payload?.id) {
        client.user = payload;
        return true;
      } else {
        return false;
      }
    }
    return false;
  }
}
