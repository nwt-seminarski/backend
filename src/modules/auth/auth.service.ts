import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { UserLoginResponseDto } from './dto/login-response.dto';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { UserService } from '../user/user.service';
import { UserJwtTokenPayload } from './types/auth.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    @InjectPinoLogger(AuthService.name) private logger: PinoLogger,
  ) {}

  async validateToken(token: string): Promise<UserJwtTokenPayload | null> {
    try {
      const payload = await this.jwtService.verifyAsync(token);
      return payload;
    } catch (e) {
      return null;
    }
  }

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.userService.findOne({ where: { username } });
    if (!user) {
      return null;
    }
    const result = await bcrypt.compare(password, user.password);
    if (result) {
      return user;
    }
  }

  async login(user: any): Promise<UserLoginResponseDto> {
    const payload: UserJwtTokenPayload = {
      username: user.username,
      id: user.id,
    };
    const ret: UserLoginResponseDto = {
      access_token: this.jwtService.sign(payload),
      userId: user.id,
      username: user.username,
    };
    return ret;
  }

  async signup(username: string, password: string): Promise<any> {
    const res = await this.userService.createUser(username, password);
    if (res === -1) {
      throw new HttpException('User already exists!', HttpStatus.FORBIDDEN);
    }
    return this.login(res);
  }
}
