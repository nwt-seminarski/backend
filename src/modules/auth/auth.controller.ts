import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiPaths, ApiAuthPaths } from 'src/constants';
import { UserLoginGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { UserJwtAuthGuard } from './jwt.guard';
import { UserLoginResponseDto } from './dto/login-response.dto';
import { AuthSignupRequestBodyDto } from './dto/signup.request.dto';

@Controller(ApiPaths.AUTH)
export class AuthApiController {
  constructor(private readonly authService: AuthService) {}

  @Post(ApiAuthPaths.LOGIN)
  @UseGuards(UserLoginGuard)
  async loginUser(@Request() req): Promise<UserLoginResponseDto> {
    return this.authService.login(req.user);
  }

  @Get(ApiAuthPaths.VERIFY)
  @UseGuards(UserJwtAuthGuard)
  async verifyToken() {
    return { data: { success: true } };
  }

  @Post(ApiAuthPaths.SIGNUP)
  async signup(@Body() { username, password }: AuthSignupRequestBodyDto) {
    return this.authService.signup(username, password);
  }
}
