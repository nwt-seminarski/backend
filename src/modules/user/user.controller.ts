import { Controller, Post } from '@nestjs/common';
import { ApiPaths, ApiUsersPaths } from 'src/constants';
import { UserService } from './user.service';

@Controller(ApiPaths.USERS)
export class UserController {
  constructor(private readonly userService: UserService) {}
}
