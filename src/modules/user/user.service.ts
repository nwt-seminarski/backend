import { Injectable } from '@nestjs/common';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { UserRepository } from '../db/repositories/user.repository';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { UserEntity } from '../db/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectPinoLogger(UserService.name) private readonly logger: PinoLogger,
    @InjectRepository(UserEntity)
    private readonly userRepository: UserRepository,
  ) {}

  async findOne(config: FindOneOptions<UserEntity>) {
    return this.userRepository.findOne(config);
  }

  async findMany(config: FindManyOptions<UserEntity>) {
    return this.userRepository.find(config);
  }

  async createUser(username: string, password: string) {
    const foundUser = await this.userRepository.findOne({
      where: {
        username,
      },
    });
    if (foundUser) {
      return -1;
    }

    const newUser = this.userRepository.create();
    newUser.username = username;
    newUser.password = password;

    return this.userRepository.save(newUser);
  }
}
