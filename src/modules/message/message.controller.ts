import { Controller } from '@nestjs/common';
import { ApiPaths } from 'src/constants';
import { MessageService } from './message.service';

@Controller(ApiPaths.MESSAGES)
export class MessageController {
  constructor(private readonly messageService: MessageService) {}
}
