export class AllUsersGetUserOutcomingDto {
  userId: number;
  username: string;
}

export class AllUsersGetOutcomingDto {
  users: AllUsersGetUserOutcomingDto[];
}
