import { IsDate, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class MessageNewOutcomingDto {
  @IsNotEmpty()
  @IsDate()
  createdAt: Date;

  @IsNotEmpty()
  @IsString()
  content: string;

  @IsNotEmpty()
  @IsString()
  username: string;

  @IsNotEmpty()
  @IsNumber()
  userId: number;
}
