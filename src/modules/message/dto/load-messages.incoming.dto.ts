import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class LoadMessagesIncomingDto {
  @IsNotEmpty()
  @IsNumber()
  userId: number;

  @IsOptional()
  @IsNumber()
  offset?: number = 0;

  @IsOptional()
  @IsNumber()
  limit?: number = 20;
}
