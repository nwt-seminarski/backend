import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';

export class InboxGetInboxOutcomingMessage {
  userId: number;
  username: string;
  numberOfNewMessages: number;
}

export class InobxGetOutcomingMessage {
  @IsNotEmpty()
  @Type(() => InboxGetInboxOutcomingMessage)
  @ValidateNested({ each: true })
  inbox: InboxGetInboxOutcomingMessage[];
}
