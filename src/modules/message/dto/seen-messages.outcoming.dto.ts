import { IsBoolean, IsNotEmpty } from 'class-validator';

export class SeenMessagesOutcomingDto {
  @IsNotEmpty()
  @IsBoolean()
  success: boolean;
}
