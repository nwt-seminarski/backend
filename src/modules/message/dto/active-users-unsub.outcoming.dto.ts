import { IsBoolean, IsNotEmpty } from 'class-validator';

export class ActiveUsersUnsubOutcomingDto {
  @IsNotEmpty()
  @IsBoolean()
  success: true;
}
