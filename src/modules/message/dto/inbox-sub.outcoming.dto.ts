import { IsBoolean, IsNotEmpty } from 'class-validator';

export class InboxSubOutcmomingMessage {
  @IsNotEmpty()
  @IsBoolean()
  success: boolean;
}
