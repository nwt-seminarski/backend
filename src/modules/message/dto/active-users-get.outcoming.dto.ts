import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';

export class ActiveUsersUserOutcomingDto {
  username: string;
  userId: number;
}

export class ActiveUsersGetOutcomingDto {
  @IsNotEmpty()
  @Type(() => ActiveUsersUserOutcomingDto)
  @ValidateNested({ each: true })
  users: ActiveUsersUserOutcomingDto[];
}
