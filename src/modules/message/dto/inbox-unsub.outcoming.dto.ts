import { IsBoolean, IsNotEmpty } from 'class-validator';

export class InboxUnsubOutcmomingMessage {
  @IsNotEmpty()
  @IsBoolean()
  success: boolean;
}
