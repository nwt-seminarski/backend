import { IsBoolean, IsNotEmpty } from 'class-validator';

export class SendMessageOutcomingDto {
  @IsNotEmpty()
  @IsBoolean()
  success: true;
}
