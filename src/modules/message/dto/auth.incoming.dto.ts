import { IsNotEmpty, IsString } from 'class-validator';

export class AuthIncomingDto {
  @IsNotEmpty()
  @IsString()
  token: string;
}
