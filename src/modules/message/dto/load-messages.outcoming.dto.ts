import { Type } from 'class-transformer';
import {
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';

export class LoadMessagesMessageOutcomingDto {
  @IsNotEmpty()
  @IsDate()
  createdAt: Date;

  @IsNotEmpty()
  @IsString()
  content: string;

  @IsNotEmpty()
  @IsString()
  username: string;

  @IsNotEmpty()
  @IsNumber()
  userId: number;
}

export class LoadMessagesOutcomingDto {
  @IsNotEmpty()
  @Type(() => LoadMessagesMessageOutcomingDto)
  @ValidateNested({ each: true })
  messages: LoadMessagesMessageOutcomingDto[];
}
