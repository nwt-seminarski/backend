import { IsBoolean, IsNotEmpty } from 'class-validator';

export class AuthOutcomingDto {
  @IsNotEmpty()
  @IsBoolean()
  success: boolean;
}
