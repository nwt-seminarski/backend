import { IsBoolean, IsNotEmpty } from 'class-validator';

export class ActiveUsersSubOutcomingDto {
  @IsNotEmpty()
  @IsBoolean()
  success: true;
}
