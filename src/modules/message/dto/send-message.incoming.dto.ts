import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class SendMessageIncomingDto {
  @IsNotEmpty()
  @IsNumber()
  userId: number;

  @IsNotEmpty()
  @IsString()
  content: string;
}
