import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsException,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { Server } from 'socket.io';
import {
  SocketEmitCommands,
  SocketIncomingCommands,
  SocketSubscriptions,
} from 'src/constants';
import { Request, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { SendMessageIncomingDto } from './dto/send-message.incoming.dto';
import { MessageService } from './message.service';
import { IncomingRequest, SocketWithUser } from 'src/common/types/request';
import { LoadMessagesIncomingDto } from './dto/load-messages.incoming.dto';
import { UserService } from '../user/user.service';
import { In, Not } from 'typeorm';
import configuration from 'src/configuration';
import { WsUserJwtAuthGuard } from '../auth/ws-auth.guard';
import { AuthOutcomingDto } from './dto/auth.outcoming.dto';
import { MessageNewOutcomingDto } from './dto/message-new.outcoming.dto';
import { SeenMessageIncomingDto } from './dto/seen-messages.incoming.dto';
import { AllUsersGetOutcomingDto } from './dto/all-users-get.outcoming.dto';

@WebSocketGateway(configuration().WS_PORT, {
  cors: {
    origin: '*',
  },
})
export class MessageGateway {
  @WebSocketServer()
  server: Server;

  private userIdToSocketId = new Map<number | null, string>();

  // each subscription contains set of user ids subscribed to events
  private subscriptions: Record<SocketSubscriptions, Set<number>> = {
    [SocketSubscriptions.ACTIVE_USERS]: new Set(),
    [SocketSubscriptions.INBOX]: new Set(),
  };

  private async updateActiveUsersSubscribers() {
    const activeUsers = await this.userService.findMany({
      where: {
        id: In(Array.from(this.userIdToSocketId.keys())),
      },
    });
    this.subscriptions['active-users'].forEach((userId) => {
      const socketId = this.userIdToSocketId.get(userId);
      if (socketId) {
        this.server.to(socketId).emit(SocketIncomingCommands.ACTIVE_USERS_GET, {
          users: activeUsers
            .map((au) => ({
              userId: au.id,
              username: au.username,
            }))
            .filter((au) => au.userId !== userId),
        });
      }
    });
  }

  constructor(
    private readonly messageService: MessageService,
    private readonly userService: UserService,
  ) {}

  @UseGuards(WsUserJwtAuthGuard)
  @SubscribeMessage(SocketIncomingCommands.AUTH)
  async auth(@ConnectedSocket() socket: SocketWithUser) {
    this.userIdToSocketId.set(socket.user.id, socket.id);
    this.activeUsersGet(socket);
    const ret: AuthOutcomingDto = {
      success: true,
    };
    socket.emit(SocketIncomingCommands.AUTH, ret);
    this.updateActiveUsersSubscribers();
  }

  handleDisconnect(socket: Socket): void {
    let userIdToRemove: number | null = null;
    for (const [key, value] of this.userIdToSocketId.entries()) {
      if (socket.id === value) {
        this.userIdToSocketId.delete(key);
        userIdToRemove = key;
      }
    }
    if (userIdToRemove) {
      this.userIdToSocketId.delete(userIdToRemove);
      this.subscriptions['active-users'].delete(userIdToRemove);
      this.subscriptions.inbox.delete(userIdToRemove);
    }
    this.updateActiveUsersSubscribers();
  }

  @UseGuards(WsUserJwtAuthGuard)
  @SubscribeMessage(SocketIncomingCommands.INBOX_SUB)
  async inboxSub(
    @ConnectedSocket() socket: SocketWithUser,
    @Request() request: IncomingRequest,
  ) {
    const userId = request.user.id;
    this.subscriptions.inbox.add(userId);
    socket.emit(SocketIncomingCommands.INBOX_SUB, { success: true });
  }

  @UseGuards(WsUserJwtAuthGuard)
  @SubscribeMessage(SocketIncomingCommands.INBOX_UNSUB)
  async inboxUnsub(
    @ConnectedSocket() socket: SocketWithUser,
    @Request() request: IncomingRequest,
  ) {
    const userId = request.user.id;
    this.subscriptions.inbox.delete(userId);
    socket.emit(SocketIncomingCommands.INBOX_UNSUB, { success: true });
  }

  @UseGuards(WsUserJwtAuthGuard)
  @SubscribeMessage(SocketIncomingCommands.ACTIVE_USERS_SUB)
  async activeUsersSub(
    @ConnectedSocket() socket: SocketWithUser,
    @Request() request: IncomingRequest,
  ) {
    const userId = request.user.id;
    this.subscriptions['active-users'].add(userId);
    socket.emit(SocketIncomingCommands.ACTIVE_USERS_SUB, { success: true });
  }

  @UseGuards(WsUserJwtAuthGuard)
  @SubscribeMessage(SocketIncomingCommands.ACTIVE_USERS_UNSUB)
  async activeUsersUnsub(
    @ConnectedSocket() socket: SocketWithUser,
    @Request() request: IncomingRequest,
  ) {
    const userId = request.user.id;
    this.subscriptions['active-users'].delete(userId);
    socket.emit(SocketIncomingCommands.ACTIVE_USERS_UNSUB, { success: true });
  }

  @UseGuards(WsUserJwtAuthGuard)
  @SubscribeMessage(SocketIncomingCommands.INBOX_GET)
  async inboxGet(
    @ConnectedSocket() socket: SocketWithUser,
    @Request() request: IncomingRequest,
  ) {
    const userId = request.user.id;
    const inbox = await this.messageService.inboxGet(userId);
    socket.emit(SocketIncomingCommands.INBOX_GET, inbox);
  }

  @UseGuards(WsUserJwtAuthGuard)
  @SubscribeMessage(SocketIncomingCommands.ACTIVE_USERS_GET)
  async activeUsersGet(@ConnectedSocket() socket: SocketWithUser) {
    const activeUsers = await this.userService.findMany({
      where: {
        id: In(
          Array.from(this.userIdToSocketId.keys()).filter(
            (userId) => userId !== socket.user.id,
          ),
        ),
      },
    });
    socket.emit(SocketIncomingCommands.ACTIVE_USERS_GET, {
      users: activeUsers.map((au) => ({
        userId: au.id,
        username: au.username,
      })),
    });
  }

  @UsePipes(new ValidationPipe())
  @UseGuards(WsUserJwtAuthGuard)
  @SubscribeMessage(SocketIncomingCommands.LOAD_MESSAGES)
  async loadMessages(
    @Request() request: IncomingRequest,
    @MessageBody() data: LoadMessagesIncomingDto,
    @ConnectedSocket() socket: SocketWithUser,
  ) {
    const userId = request.user.id;
    const ret = await this.messageService.loadMessages(
      userId,
      data.userId,
      data.limit,
      data.offset,
    );
    socket.emit(SocketIncomingCommands.LOAD_MESSAGES, ret);
  }

  @UseGuards(WsUserJwtAuthGuard)
  @SubscribeMessage(SocketIncomingCommands.ALL_USERS_GET)
  async allUsersGet(
    @ConnectedSocket() socket: SocketWithUser,
    @Request() request: IncomingRequest,
  ) {
    const userId = request.user.id;
    const allUsers = await this.userService.findMany({
      where: {
        id: Not(userId),
      },
    });
    const ret: AllUsersGetOutcomingDto = {
      users: allUsers.map((us) => ({ userId: us.id, username: us.username })),
    };
    socket.emit(SocketIncomingCommands.ALL_USERS_GET, ret);
  }

  @UsePipes(new ValidationPipe())
  @UseGuards(WsUserJwtAuthGuard)
  @SubscribeMessage(SocketIncomingCommands.SEEN_MESSAGES)
  async seenMessages(
    @ConnectedSocket() socket: SocketWithUser,
    @Request() request: IncomingRequest,
    @MessageBody() body: SeenMessageIncomingDto,
  ) {
    const userId = request.user.id;
    const success = await this.messageService.seenMessages(userId, body.userId);
    socket.emit(SocketIncomingCommands.SEEN_MESSAGES, { success });
  }

  @UsePipes(new ValidationPipe())
  @UseGuards(WsUserJwtAuthGuard)
  @SubscribeMessage(SocketIncomingCommands.SEND_MESSAGE)
  async sendMessageListener(
    @Request() request: IncomingRequest,
    @MessageBody() data: SendMessageIncomingDto,
    @ConnectedSocket() socket: SocketWithUser,
  ) {
    const userId = request.user.id;
    const subscribedToInbox = this.subscriptions.inbox.has(data.userId);
    const res = await this.messageService.sendMessage(
      userId,
      data.userId,
      data.content,
      false,
    );
    if (typeof res === 'number') {
      throw new WsException(`Something went wrong! Code (${res})`);
    }
    if (subscribedToInbox) {
      const socketToSendTo = this.userIdToSocketId.get(data.userId);
      if (socketToSendTo) {
        const newMessageData: MessageNewOutcomingDto = {
          userId: res.fromUser.id,
          username: res.fromUser.username,
          content: res.content,
          createdAt: res.createdAt,
        };
        this.server.sockets
          .to(socketToSendTo)
          .emit(SocketEmitCommands.MESSAGES_NEW, newMessageData);
        const inboxList = await this.messageService.inboxGet(data.userId);
        this.server.sockets
          .to(socketToSendTo)
          .emit(SocketIncomingCommands.INBOX_GET, inboxList);
      }
    }
    socket.emit(SocketIncomingCommands.SEND_MESSAGE, { success: true });
  }
}
