import { Injectable } from '@nestjs/common';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { MessageRepository } from '../db/repositories/message.repository';
import { UserService } from '../user/user.service';
import { IsNull } from 'typeorm';
import { InobxGetOutcomingMessage } from './dto/inbox-get.outcoming.dto';
import { LoadMessagesOutcomingDto } from './dto/load-messages.outcoming.dto';
import { MessageEntity } from '../db/entities/message.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class MessageService {
  constructor(
    @InjectPinoLogger(MessageService.name) private readonly logger: PinoLogger,
    @InjectRepository(MessageEntity)
    private readonly messageRepository: MessageRepository,
    private readonly userService: UserService,
  ) {}

  async seenMessages(userId: number, withUserId: number): Promise<boolean> {
    try {
      const unseenMessages = await this.messageRepository.find({
        where: {
          toUser: {
            id: userId,
          },
          fromUser: {
            id: withUserId,
          },
        },
      });
      await this.messageRepository.save(
        unseenMessages.map((um) => ({ ...um, seenAt: new Date() })),
      );
      return true;
    } catch (e) {
      return false;
    }
  }

  async loadMessages(
    userId: number,
    fromUserId: number,
    limit = 100,
    offset = 0,
  ): Promise<LoadMessagesOutcomingDto> {
    const ret: LoadMessagesOutcomingDto = {
      messages: [],
    };

    const [messagesIncoming, messagesOutcoming] = await Promise.all([
      this.messageRepository.find({
        where: {
          toUser: {
            id: userId,
          },
          fromUser: {
            id: fromUserId,
          },
        },
        skip: offset,
        take: limit,
        relations: ['fromUser'],
      }),
      this.messageRepository.find({
        where: {
          toUser: {
            id: fromUserId,
          },
          fromUser: {
            id: userId,
          },
        },
        skip: offset,
        take: limit,
        relations: ['fromUser'],
      }),
    ]);

    const allMessages = messagesIncoming
      .concat(messagesOutcoming)
      .sort((a, b) => {
        return a.createdAt.getTime() - b.createdAt.getTime();
      });

    ret.messages = allMessages.concat().map((msg) => ({
      content: msg.content,
      createdAt: msg.createdAt,
      userId: msg.fromUser.id,
      username: msg.fromUser.username,
    }));

    return ret;
  }

  async inboxGet(userId: number): Promise<InobxGetOutcomingMessage> {
    const ret: InobxGetOutcomingMessage = {
      inbox: [],
    };
    const unSeenMessages = await this.messageRepository.find({
      where: {
        toUser: {
          id: userId,
        },
        seenAt: IsNull(),
      },
      relations: ['fromUser'],
    });

    unSeenMessages.forEach((um) => {
      const hasThisUserInInbox = ret.inbox.find(
        (inb) => inb.userId === um.fromUser.id,
      );
      if (!hasThisUserInInbox) {
        ret.inbox.push({
          userId: um.fromUser.id,
          username: um.fromUser.username,
          numberOfNewMessages: 1,
        });
      } else {
        hasThisUserInInbox.numberOfNewMessages += 1;
      }
    });

    return ret;
  }

  async sendMessage(
    fromUserId: number,
    toUserId: number,
    content: string,
    seen = false,
  ) {
    const [fromUser, toUser] = await Promise.all([
      this.userService.findOne({
        where: {
          id: fromUserId,
        },
      }),
      this.userService.findOne({
        where: {
          id: toUserId,
        },
      }),
    ]);

    if (!fromUser) {
      return -1;
    }
    if (!toUser) {
      return -2;
    }

    const newMessageRec = this.messageRepository.create();
    newMessageRec.fromUser = fromUser;
    newMessageRec.toUser = toUser;
    newMessageRec.content = content;
    newMessageRec.seenAt = seen ? new Date() : null;

    const savedMessage = await this.messageRepository.save(newMessageRec);

    if (!savedMessage) {
      return -3;
    }

    return savedMessage;
  }
}
