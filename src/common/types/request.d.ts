import { Request } from 'express';
import { UserJwtTokenPayload } from 'src/modules/auth/types/auth.service';
import { Socket } from 'socket.io';

export interface IncomingRequest extends Request {
  user: UserJwtTokenPayload;
}

export interface SocketWithUser extends Socket {
  user: UserJwtTokenPayload;
}
