FROM node:18.9-alpine

WORKDIR /server
COPY package*.json ./
COPY *.tgz ./
RUN npm i --force

RUN apk --no-cache add curl

COPY . ./
RUN npm run build

RUN mkdir -p ./${IMAGE_DIRECTORY}

EXPOSE 3000
CMD ["node", "./dist/src/main.js"]
